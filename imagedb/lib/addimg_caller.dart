import 'package:flutter/material.dart';
import 'addimg_page.dart';
class AddImgCaller extends StatelessWidget {
  const AddImgCaller({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      //debugShowCheckedModeBanner: false,
      //title: 'Pantalla "expanded"',
      home: AddImgPage(),
    );
  }
}