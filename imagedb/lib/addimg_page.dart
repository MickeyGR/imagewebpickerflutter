import 'dart:convert';
import 'dart:io' as io;
import 'dart:io';
import 'dart:typed_data';
import 'package:dio/dio.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class AddImgPage extends StatefulWidget {
  @override
  _AddImgPageState createState() => _AddImgPageState();
}

class _AddImgPageState extends State<AddImgPage> {
  File? image;
  io.File? imagebd;
  Map? data;
  dynamic data2;
  List? datalist;
  TextEditingController? _nombre;
  String imageData = "", imgbd = "";
  List? imgList;
  final dio = Dio(BaseOptions());
  Uint8List? imgnut8;
  PlatformFile? img;
  List<int>? _selectedFile;

  getImages() async {
    String url = 'http://localhost:3000/images';
    var res = await http.get(Uri.parse(url));
    data = json.decode(res.body);
    data2 = json.decode(res.body);

    setState(() {
      imgList = data!['response'];
      imageData = data.toString();
    });
  }

  upload(Uint8List byteimg) async {
    this._selectedFile = byteimg;
    // string to uri
    var uri = Uri.parse("http://localhost:3005/post_img");

    // create multipart request
    var request = new http.MultipartRequest("POST", uri);
    request.fields['user'] = 'someone@somewhere.com';
    var multipartFile = new http.MultipartFile.fromBytes('foto', _selectedFile!,
        filename: this.img!.name);
    // add file to multipart
    request.files.add(multipartFile);

    // send request to upload image
    await request.send().then((response) async {
      // listen for response
      response.stream.transform(utf8.decoder).listen((value) {
        print(value);
      });
    }).catchError((e) {
      print(e);
    });
  }

  @override
  void initState() {
    _nombre = TextEditingController();
    super.initState();
    getImages();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      child: Row(
        children: [
          Expanded(flex: 2, child: Container()),
          Expanded(
            flex: 3,
            child: Card(
                margin: EdgeInsets.all(50),
                child: Column(
                  children: [
                    Center(
                        child: Stack(
                      alignment: Alignment.bottomRight,
                      children: [_img(), _cambiarImg()],
                    )),
                    Center(
                        child: Container(
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Expanded(flex: 2, child: Container()),
                              Expanded(
                                flex: 3,
                                child: TextField(
                                  controller: _nombre,
                                  decoration:
                                      InputDecoration(labelText: "Nombre"),
                                  style: TextStyle(fontSize: 20),
                                ),
                              ),
                              Expanded(flex: 2, child: Container()),
                            ],
                          ),
                          _saveBoton(),
                          /* Text("${base64.decode(imgList![32]["img"])}") */
                          Text(imageData),
                          //_imgbd()
                        ],
                      ),
                    )),
                  ],
                )),
          ),
          Expanded(flex: 2, child: Container()),
        ],
      ),
    ));
  }

  Widget _img() {
    if (this.imageData.isNotEmpty) {
      return Padding(
          padding: const EdgeInsets.only(top: 50, bottom: 20),
          child: Image.memory(imgnut8!,
              width: 300, height: 300, fit: BoxFit.cover));
    } else {
      return Padding(
        padding: const EdgeInsets.only(top: 50, bottom: 20),
        child: FlutterLogo(size: 300),
      );
    }
  }

  Widget _cambiarImg() {
    return IconButton(
      icon: Icon(Icons.refresh),
      color: Colors.green,
      onPressed: () => _buscarImagen(),
    );
  }

  Future _buscarImagen() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: ['jpg', 'png', 'doc'],
    );
    if (result != null) {
      PlatformFile file = result.files.first;
      print(file.extension);
      setState(() => {
            this.imageData = file.name,
            this.img = file,
            this.imgnut8 = file.bytes
          });
    }
  }

  Widget _saveBoton() {
    return Container(
        margin: EdgeInsets.all(8),
        child: ElevatedButton(
          child: Text("Guardar"),
          onPressed: () {
            upload(this.imgnut8!);
          },
        ));
  }
}
