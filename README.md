# Mickey GR
# ImageWebPickerFlutter

Uso de FilePicker en Flutter WEB

## ¿Cómo probar el proyecto?

Una vez decargado entra a la carpeta del proyecto
imagedb, hace un "flutter clean" y luego un "flutter pub get"

```bash
cd ImagePickerFlutter
cd imagedb
flutter clean
flutter pub get
```

Luego puesdes correr el proyecto con cualquier navegador, por ejemplo Firefox con:
```bash
flutter run -d web-server
```
o
```bash
flutter run -d chrome
```
o
```bash
flutter run
```
